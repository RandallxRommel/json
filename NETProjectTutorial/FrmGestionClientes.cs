﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Microsoft.ReportingServices.Interfaces;
using NETProjectTutorial.entities;
using NETProjectTutorial.model;

namespace NETProjectTutorial
{
    public partial class FrmGestionClientes : Form
    {
        private DataSet dsClientes;
        private BindingSource bsClientes;
        private int index;
        
        public FrmGestionClientes()
        {
            InitializeComponent();
            bsClientes = new BindingSource();
        }

        public DataSet DsClientes
        {
            set
            {
                dsClientes = value;
            }
        }

        private void FrmGestionClientes_Load(object sender, EventArgs e)
        {
            bsClientes.DataSource = dsClientes;
            bsClientes.DataMember = dsClientes.Tables["Cliente"].TableName;
            dgvClientes.DataSource = bsClientes;
            dgvClientes.AutoGenerateColumns = true;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            FrmCliente frmCliente = new FrmCliente();
            frmCliente.TblClientes = dsClientes.Tables["Cliente"];
            frmCliente.DsClientes = dsClientes;
            frmCliente.ShowDialog();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvClientes.SelectedRows;
            int id = int.Parse(dgvClientes.Rows[index].Cells["ID"].FormattedValue.ToString());

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            FrmCliente fp = new FrmCliente();
            fp.TblClientes = dsClientes.Tables["Cliente"];
            fp.DsClientes = dsClientes;
            fp.DrCliente = drow;

            ClienteModel.GetListClientes().ForEach(c =>
            {
                if (c.id.Equals(id))
                {
                    fp.SaveCliente(c);
                }
            });

            fp.ShowDialog();
        }

        private void dgvClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvClientes.SelectedRows;
            int id = int.Parse(dgvClientes.Rows[index].Cells["ID"].FormattedValue.ToString());

            Console.WriteLine(id);

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;


            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                dsClientes.Tables["Cliente"].Rows.Remove(drow);
                foreach(Cliente c in ClienteModel.GetListClientes())
                {
                    if(c.id == id)
                    {
                        ClienteModel.removeCliente(c);
                        break;
                    }
                }    
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtFinder_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsClientes.Filter = string.Format("Nombres like '*{0}*' or Apellidos like '*{0}*' or Telefono like '*{0}*' or Cedula like '*{0}*' or Direccion like '*{0}*'  ", txtFinder.Text);
            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
