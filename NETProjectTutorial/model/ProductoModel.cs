﻿using Nancy.Json;
using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ProductoModel
    {
        private static  List<Producto> productos = new List<Producto>();


        public static List<Producto> GetListProductos()
        {
            return productos;
        }

        public static void Populate()
        {
            
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            productos = serializer.Deserialize<List<Producto>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.ProductosData));

        }
    }
}
