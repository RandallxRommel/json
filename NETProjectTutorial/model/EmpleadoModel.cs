﻿using Nancy.Json;
using NETProjectTutorial.entities;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class EmpleadoModel
    {
        private static List<Empleado> Lstempleados = new List<Empleado>();

        public static List<Empleado> GetListEmpleado()
        {
            return Lstempleados;
        }

        public static void Populate()
        {
            
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Lstempleados = serializer.Deserialize<List<Empleado>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.EmpleadosData));
          
        }
    }
}
