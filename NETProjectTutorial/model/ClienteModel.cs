﻿using Nancy.Json;
using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ClienteModel
    {
        private static List<Cliente> Clientes = new List<Cliente>();

        static string filePath = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory())) + @"\Resources\ClientesData.json";


        public static List<Cliente> GetListClientes()
        {
            return Clientes;
        }

        public static void LoadFromJson()
        {
            string file = File.ReadAllText(filePath);
            Clientes = JsonConvert.DeserializeObject<List<Cliente>>(file);
        }

        public static void UpdateJson()
        {
            string texto = JsonConvert.SerializeObject(Clientes);
            File.WriteAllText(filePath, texto);
        }   

        public static void addCliente(Cliente c)
        {
            Clientes.Add(c);
        }

        public static void removeCliente(Cliente c)
        {
            Clientes.Remove(c);
        }
    }
}
