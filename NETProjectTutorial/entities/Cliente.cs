﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy.ViewEngines;

namespace NETProjectTutorial.entities
{
    public class Cliente
    {
        public int id { get; set; }
        public string cedula { get; set; }
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public SEXO sexo { get; set; }

        public Cliente()
        {
        }

        public Cliente(int id, string cedula, string nombres, string apellidos, string direccion, string telefono, SEXO sexo)
        {
            this.id = id;
            this.cedula = cedula;
            this.nombres = nombres;
            this.apellidos = apellidos;
            this.direccion = direccion;
            this.telefono = telefono;
            this.sexo = sexo;
        }

        public enum SEXO
        {
            Male, Female
        }

        public override string ToString()
        {
            return base.ToString();
        }

    }
}
