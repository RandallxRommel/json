﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    public class ProductoFactura
    {
        private string sku;
        private string nombre;
        private int cantidad;
        private double precio;
        private string empleado;
        private string cliente;

        public ProductoFactura(string sku, string nombre, int cantidad, double precio, string empleado, string cliente)
        {
            this.sku = sku;
            this.nombre = nombre;
            this.cantidad = cantidad;
            this.precio = precio;
            this.empleado = empleado;
            this.cliente = cliente;
        }

        public string Empleado{ get => empleado; set => empleado = value; }
        public string Cliente { get => cliente; set => cliente = value; }
        public string Sku { get => sku; set => sku = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public int Cantidad { get => cantidad; set => cantidad = value; }
        public double Precio { get => precio; set => precio = value; }
    }
}
