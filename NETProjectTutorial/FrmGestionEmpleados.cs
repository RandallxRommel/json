﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionEmpleados : Form
    {
        private DataSet dsEmpleados;
        private BindingSource bsEmpleados;
        
        public FrmGestionEmpleados()
        {
            InitializeComponent();
            bsEmpleados = new BindingSource();
        }

        public DataSet DsEmpleados
        {
            set
            {
                dsEmpleados = value;
            }
        }

        private void FrmGestionEmpleados_Load(object sender, EventArgs e)
        {
            bsEmpleados.DataSource = dsEmpleados;
            bsEmpleados.DataMember = dsEmpleados.Tables["Empleado"].TableName;
            dgvEmpleados.DataSource = bsEmpleados;
            dgvEmpleados.AutoGenerateColumns = true;
        }

        private void txtFinder_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
