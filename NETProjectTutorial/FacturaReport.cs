﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FacturaReport : Form
    {
        private DataSet dsSistema;
        private BindingSource bsFactura;
        private BindingSource bsProductoFactura;

        public FacturaReport()
        {
            InitializeComponent();
            bsFactura = new BindingSource();
            bsProductoFactura = new BindingSource();
        }

        public DataSet DsSistema { set => dsSistema = value; }

        private void FacturaReport_Load(object sender, EventArgs e)
        {
            bsFactura.DataSource = dsSistema;
            bsFactura.DataMember = "Factura";
            bsProductoFactura.DataSource = dsSistema;
            bsProductoFactura.DataMember = "ProductoFactura";

            //Validar que la factura y el detalle sean los correctos a mostrar

            this.reportViewer1.LocalReport.ReportEmbeddedResource = "NETProjectTutorial.FacturaReport.rdlc";
            ReportDataSource rds1 = new ReportDataSource("dsFactura",dsSistema.Tables["Factura"]);
            ReportDataSource rds2 = new ReportDataSource("dsProductoFactura", dsSistema.Tables["ProductoFactura"]);
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(rds1);
            this.reportViewer1.LocalReport.DataSources.Add(rds2);

            this.reportViewer1.RefreshReport();
        }
    }
}
