﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NETProjectTutorial.entities;
using NETProjectTutorial.model;

namespace NETProjectTutorial
{
    public partial class FrmCliente : Form
    {
        private DataTable tblClientes;
        private DataSet dsClientes;
        private BindingSource bsClientes;
        private DataRow drClientes;
        public DataTable TblClientes { set { tblClientes = value; } }
        public DataSet DsClientes { set { dsClientes = value; } }
        private Cliente editCliente = new Cliente();

        public DataRow DrCliente
        {
            set
            {
                drClientes = value;
                txtId.Text = drClientes["ID"].ToString();
                txtCedula.Text = drClientes["Cedula"].ToString();
                txtNombre.Text = drClientes["Nombres"].ToString();
                txtApellido.Text = drClientes["Apellidos"].ToString();
                txtBDirecion.Text = drClientes["Direccion"].ToString();
                txtTelefono.Text = drClientes["Telefono"].ToString();
                rdButtonMasculino.Text = drClientes["Sexo"].ToString();
                rdButtonFemenino.Text = drClientes["Sexo"].ToString();

            }
        }

        public FrmCliente()
        {
            InitializeComponent();
            bsClientes = new BindingSource();
        }


        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if(rdButtonMasculino.Checked)
            {
                rdButtonMasculino.Text = "Masculino";
                rdButtonFemenino.Enabled = false;
            }
        }

        private void rdButtonFemenino_CheckedChanged(object sender, EventArgs e)
        {
            if (rdButtonFemenino.Checked)
            {
                rdButtonFemenino.Text = "Femenino";
                rdButtonMasculino.Enabled = false;
            }
        }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            bsClientes.DataSource = dsClientes;
            bsClientes.DataMember = dsClientes.Tables["Cliente"].TableName;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {  

            if (drClientes != null)
            {
                DataRow drNew = tblClientes.NewRow();

                int index = tblClientes.Rows.IndexOf(drClientes);
                drNew["ID"] = drClientes["ID"];
                drNew["Cedula"] = txtCedula.Text;
                drNew["Nombres"] = txtNombre.Text;
                drNew["Apellidos"] = txtApellido.Text;
                drNew["Direccion"] = txtBDirecion.Text;
                drNew["Telefono"] = txtTelefono.Text;

                editCliente.id = Convert.ToInt32(txtId.Text);
                editCliente.cedula = txtCedula.Text;
                editCliente.nombres = txtNombre.Text;
                editCliente.apellidos = txtApellido.Text;
                editCliente.direccion = txtBDirecion.Text;
                editCliente.telefono = txtTelefono.Text;
                
                if(rdButtonMasculino.Checked)
                {
                    drNew["Sexo"] = Cliente.SEXO.Male;
                    editCliente.sexo = Cliente.SEXO.Male;
                } else
                {
                    drNew["Sexo"] = Cliente.SEXO.Female;
                    editCliente.sexo = Cliente.SEXO.Female;
                }

                tblClientes.Rows.RemoveAt(index);
                tblClientes.Rows.InsertAt(drNew, index);
            } else
            {
                tblClientes.Rows.Add(tblClientes.Rows.Count + 1, txtCedula.Text, txtNombre.Text, txtApellido.Text, txtBDirecion.Text, txtTelefono.Text, editCliente.sexo.ToString());
                Cliente c = new Cliente(tblClientes.Rows.Count + 1, txtCedula.Text, txtNombre.Text, txtApellido.Text, txtBDirecion.Text, txtTelefono.Text, editCliente.sexo);
                ClienteModel.GetListClientes().Add(c);

            }

            Dispose();
        }

        public void SaveCliente(Cliente c)
        {
            this.editCliente = c;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void txtId_TextChanged(object sender, EventArgs e)
        {   
            txtId.Text = Convert.ToString(tblClientes.Rows.Count + 1);
            txtId.Enabled = false;
        }
    }
}
