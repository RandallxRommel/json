## Sistemático de programación II 28-06-2020 Valor 15pts.

Integrantes:
- Dewin Alexander Umaña Hernández
- Randall Agustín Hodgson

Docente:
**Yasser Membreño**

## Actividades
## Clonar el proyecto netprojecttutorial del repositorio

1. Agregar la entidad para Cliente

2. Agregar la tabla en el dataset

3. Diseñar la ventana de gestión de cliente

4. Implementar los botones crear, editar, eliminar y la búsqueda en el campo de texto

5. Diseñar la ventana para cliente nuevo y editar cliente. (Es un sola ventana).

6. Agregar el combobox para el cliente en la ventana de factura

7. Implementar búsqueda en el combobox de cliente y producto

8. En el reporte de facturación agregar los datos del cliente y empleado

